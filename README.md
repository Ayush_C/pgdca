# Computer Fundamental

## Q1. data access method
-----------------------------

> Data Access Methods

How data files are stored in secondary storage varies with the types of media and devices you are using. Data files may be stored on or in sequential-access storage, direct-access storage, or random-access storage. 

1. **Sequential Access Storage** - Punched cards, paper tape, and magnetic tape are examples of sequential-access storage media. When operating in a sequential environment, a particular record can be read only by first reading all the records that come before it in the file. When you store a file on tape, the 125th record cannot be read until the 124 records in front of it are read. The records are read in sequence. You cannot read just any record at random. This is also true when reading punched cards or paper tape.

2. **Direct Access Storage** - Direct-access storage allows you to access the 125th record without first having to read the 124 records in front of it. Magnetic disks and drums are examples of direct-access storage media. Data can be obtained quickly from anywhere on the media. However, the amount of time it takes to access a record is dependent to some extent on the mechanical process involved. It is usually necessary to scan some (but not all) of the preceding data.

3. **Random Access Storage** -  Random-access storage media refers to magnetic core, semiconductor, thin film, and bubble storage. Here, a given item of data can be selected from anywhere in storage without having to scan any preceding items. And, the access time is independent of the storage location.

In computing, an access method is a program or a hardware mechanism that moves data between the computer and an outlying device such as a hard disk (or other form of storage) or a display terminal. The term is sometimes used to refer to the mechanics of placing or locating specific data at a particular place on a storage medium and then writing the data or reading it. It is also used to describe the way that data is located within a larger unit of data such as a data set or file. An access method is also an application program interface (API) that a programmer uses to create or access data sets or to read from or write to a display terminal or other output device. Examples are the Virtual Sequential Access Method (VSAM) and the Virtual Telecommunication Access Method (VTAM).

<br>

## Q2. (1) Monitor (2) Monochrome (3) Color Monitor
------------------------------------

### 1. Monitor

A monitor is an electronic visual computer display that includes a screen, circuitry and the case in which that circuitry is enclosed. Older computer monitors made use of cathode ray tubes (CRT), which made them large, heavy and inefficient. Nowadays, flat-screen LCD monitors are used in devices like laptops, PDAs and desktop computers because they are lighter and more energy efficient.

A monitor is also known as a screen or a visual display unit (VDU). 

>The quality of a monitor's performance is assessed using a few key factors:

- Aspect Ratio: This is the relation of the vertical length to the horizontal length of the monitor (e.g. 16:9 or 4:5).
- Dot Pitch: This is the distance between each pixel in every square inch that's displayed. The shorter the distance, the sharper and clearer the images are.
- Display Resolution: Also known as dots per inch (DPI), this determines the number of pixels per linear inch. The maximum number of pixels is determined by the dot pitch. This determines the number of pixels the display screen can accommodate.
- Size: This aspect is determined by the display screen’s diagonal measurement.

### 2. Monochrome

Monochrome is a legacy computer display system that only displays one or two colors with several shades. Monochrome refers to early computer monitors that were used from the very first computers until color monitors emerged in the 1980s. Monochrome monitors are now very rare.

Monochrome relies on two main colors - often black and white - as well as all the shades in between. The colors in a monochromatic monitor depend on the type of phosphorus used in the monitor's display tube. In computer monitors, this color was mainly green, although a few monitors included red or white. Besides computers, the monochrome display was implemented in other devices as well such as cash counters, information kiosks and laboratory equipment displays. 

### 3. Color Monitor

A display monitor capable of displaying many colors. In contrast, a monochrome monitor can display only two colors — one for the background and one for the foreground. Color monitors implement the RGB color model by using three different phosphors that appear red, green, and blue when activated. By placing the phosphors directly next to each other, and activating them with different intensities, color monitors can create an unlimited number of colors. In practice, however, the real number of colors that any monitor can display is controlled by the video adapter.

*Color monitors based on CRT technology employ three different techniques to merge phosphor triplets into pixels:*

- **Dot-trio shadow masks** place a thin sheet of perforated metal in front of the screen. Since electrons can pass only through the holes in the sheet, each hole represents a single pixel.
- **Aperture-grille CRTs** place a grid of wires between the screen and the electron guns.
- **Slot-mask CRTs** uses a shadow mask but the holes are long and thin. It’s sort of a cross between the dot-trio shadow mask and aperture-grill techniques.

<br>

## Q3. Application of internet
------------------------------------------

> Introduction to Internet Application

Internet Applications can be described as the type of applications that use the internet for operating successfully, that is, by using the internet for fetching, sharing and displaying the information from the respective server systems. It can be accessed only with the help of the internet facility, and it cannot be functional without the internet. These applications can be classified as electronic devices based, automated digital technology, industrial internet, smartphones based, smart home-based, smart grids, smart city, and other major applications.

> Services of Internet Application

- The internet has many few major applications like electronic mail services, web browsing, peer to peer networking. The use of email increases because of its several features like attachments, messages, data usage.
- The attachment feature such as word documents, excel sheets, and graphical media is possible because of Multipurpose Internet Mail Extensions, but the result is traffic volume caused by mail is calibrated in terms of data packets in the network.
- Electronic mail services became a vital part of personal and professional communication method, and its time and cost consuming. The data is transmitted and received securely by encryption. The price of tickets for transport and sport are received in the mail.
- The web browser is a critical application of the internet and is highly commercial dominated by Microsoft and highly influenced by WWW – World Wide Web.
- The web browser is free and available as an open-source model that enriches the minds of future generations. The open-source has been developed and deployed on a modular basis since the source code is accessible only with few usage restrictions. The open-source feature has been integrated to file managers and web browsers.
- Other important applications and potentially needed in Internet application is peer-to-peer networking.
- This P2P networking is a dynamic method that is based on the exchanging of physical resources like hard drives, files, processors and other intelligent features.
- Each group of peer to peer networking has equal responsibility and functions. Peer-to-peer applications based on the internet locate the computer at the focus of the computing matrix based on cross-network protocols like SOAP Simple Object Access Protocol or Remote Procedure Calling XML-RPC the user to enter on the Internet more proactively.

> Top Application of Internet

1. Smart Home
2. Electronic Devices
3. Automated Digital Technology
4. Industrial Internet
5. Smart City
6. Smartphones
7. Smart Grids
8. Major Application

> Advantages of the internet

* The internet is a suitable environment to work with people all over the world through instant communication that can provide products and services easier and faster.
* An internet connection made the employees work from the option to create a virtual office at home.
* The internet connection connects your laptop or pc to internet aided devices to access cloud computing and cloud storage.
* The internet can build a supercomputer to perform and manage complex task.

<br>

## Q4. Types of computer viruses
-----------------------------------------

A computer virus is a program which can harm our device and files and infect them for no further use. When a virus program is executed, it replicates itself by modifying other computer programs and instead enters its own coding. This code infects a file or program and if it spreads massively, it may ultimately result in crashing of the device. 

**HOW TO IDENTIFY IF YOUR COMPUTER IN UNDER A VIRUS ATTACK**

* **Speed of the System** – In case a virus is completely executed into your device, the time taken to open applications may become longer and the entire system processing may start working slowly.
* **Pop-up Windows** – One may start getting too many pop up windows on their screen which may be virus affected and harm the device even more.
* **Self Execution of Programs** – Files or applications may start opening in the background of the system by themselves and you may not even know about them.
* **Log out from Accounts** – In case of a virus attack, the probability of accounts getting hacked increase and password protected sites may also get hacked and you might get logged out from all of them.
* **Crashing of the Device** – In most cases, if the virus spreads in maximum files and programs, there are chances that the entire device may crash and stop working.

> Types of Computer Virus:

- **Boot Sector Virus** – It is a type of virus that infects the boot sector of floppy disks or the Master Boot Record (MBR) of hard disks. The Boot sector comprises all the files which are required to start the Operating system of the computer. The virus either overwrites the existing program or copies itself to another part of the disk.
- **Direct Action Virus** – When a virus attaches itself directly to a .exe or .com file and enters the device while its execution is called a Direct Action Virus. If it gets installed in the memory, it keeps itself hidden. It is also known as Non-Resident Virus.
- **Resident Virus** – A virus which saves itself in the memory of the computer and then infects other files and programs when its originating program is no longer working. This virus can easily infect other files because it is hidden in the memory and is hard to be removed from the system.
- **Multipartite Virus** – A virus which can attack both, the boot sector and the executable files of an already infected computer is called a multipartite virus. If a multipartite virus attacks your system, you are at risk of cyber threat.
- **Overwrite Virus** – One of the most harmful viruses, the overwrite virus can completely remove the existing program and replace it with the malicious code by overwriting it. Gradually it can completely replace the host’s programming code with the harmful code.
- **Polymorphic Virus** – Spread through spam and infected websites, the polymorphic virus are file infectors which are complex and are tough to detect. They create a modified or morphed version of the existing program and infect the system and retain the original code.
- **File Infector Virus** – As the name suggests, it first infects a single file and then later spreads itself to other executable files and programs. The main source of this virus are games and word processors.
- **Spacefiller Virus** – It is a rare type of virus which fills in the empty spaces of a file with viruses. It is known as cavity virus. It will neither affect the size of the file nor can be detected easily.
- **Macro Virus** – A virus written in the same macro language as used in the software program and infects the computer if a word processor file is opened. Mainly the source of such viruses is via emails.

<br>

# PC Package

## Q1. Introduction to MS windows
------------------------------------

Microsoft Windows is a multitasking operating system developed by Microsoft Corporation which uses Graphical User Interface to interact with the users. Microsoft was originally named “Traf-O-Data” in 1972, was renamed as “Micro-soft” in November 1975, then “Microsoft” on November 26, 1976. Microsoft entered the marketplace in August 1981 by releasing version 1.0 of the operating system Microsoft DOS (MS-DOS), a 16-bit command-line operating system. Bill Gates and Paul Allen founded Microsoft and windows operating system has been its primary product.
```
Windows 1.0 – Nov 1985
Windows 2.0 – Dec 1987
Windows 3.0 – May 1990
Windows 95 – Aug 1995
Windows 98 – June 1998
Windows ME – Sep 2000
Windows XP – Oct 2001
Windows Vista – Nov 2006
Windows 7 – July 2009
Windows 8.0 – Oct 2012
Windows 8.1 – Oct 2013
Windows 10 – July 2015
Windows 11 - Oct 2021
```

> Features of Windows


* Windows Search: We can have numerous files and contents located on our system and sometimes we may run out of memory about the exact location of our file. Windows Search is a search function included with Windows that allows the user to search their entire computer

* Windows File Transfer: We may have the need to transfer in or transfer out the files and contents from our machine to other devices such as other computers or mobiles and tablets. We can do this by using an Easy Transfer Cable, CDs or DVDs, a USB flash drive, wireless Bluetooth, a network folder, or an external hard disk.

* Windows Updates: Windows includes an automatic update feature with the intended purpose of keeping its operating system safe and up-to-date.

* Windows taskbar: At the bottom most part of your windows, you will see a row which is known as the taskbar. It has the currently running applications, you can also pin applications that you frequently use by using an option Pin to Taskbar”. The taskbar is the main navigation tool for Windows

* Remote Desktop Connection: This feature of windows allows you to connect to another system and work remotely on another system.

> Advantages of Windows

- Desktop as well as tablet-friendly OS
- Switch between applications is very easy
- Not much technical knowledge is required to operate windows
- Windows OS is the dominant OS and enjoys more than 90% of Market share
- MS OS have a great support community and it also has the largest number of applications
- Microsoft provides a powerful set of Enterprise focused Operating System, Applications and the services making it the most dominant player in the OS market.

> Disadvantages of Windows

- Cost for upgrade
- Windows OS attracts a large number of virus programs due to its largest market share and easy to breach paradigm
- Windows OS is not that much of touch-friendly

<br>

## Q2. What is a word processing
------------------------------------

Word processing is an application program that allows you to create letters, reports,newsletters, tables, form letters, brochures, and Web pages. Using this application program you can add pictures, tables, and charts to your documents. You can also check spelling and grammar.

A word processor is an electronic device or computer application software that performs word processing: the composition, editing, formatting and sometimes printing of any sort of written material. Word processing can also refer to advanced shorthand techniques, sometimes used in specialized contexts with a specially modified typewriter. The term was coined at IBM's Böblingen, West Germany Laboratory in the 1960s. Typical features of a word processor include font application, spell checking, grammar checking, a built-in thesaurus, automatic text correction, Web integration and HTML exporting, among others.

> Main features of word processing applications:

- Create professional documents fast, using built-in and custom templates.
- Easily manage large documents using various features like the ability to create table of contents, index, and cross-references.
- Work on multiple documents simultaneously.
- With the help of mail merge, you can quickly create merge documents like mass mailings or mailing labels.
- AutoCorrect and AutoFormat features catch typographical errors automatically and allow you to use predefined shortcuts and typing patterns to quickly format your documents.
- The print zoom facility scales a document on different paper sizes, and allows you to print out multiple pages on a single sheet of paper.
- The nested tables feature supports putting one table inside another table.
- Export and save your word documents in PDF and XPS file format.
- Batch mailings using form letter template and an address database (also called mail merging);
- Indices of keywords and their page numbers;
- Tables of contents with section titles and their page numbers;
- Tables of figures with caption titles and their page numbers;
- Cross-referencing with section or page numbers;
- Footnote numbering;
- New versions of a document using variables (e.g. model numbers, product names, etc.

<br>

## Q3. Difference between find and replace
-----------------------------------------------

Find and Replace helps you to find words or formats in a document and can let you replace all instances of a word or format. This is particularly handy in long documents.

To use Find and Replace, use the shortcut Ctrl+H or navigate to Editing in the Home tab of the ribbon, then choose Replace. To just quickly find something, use the shortcut Ctrl+F or navigate to Home>Editing>Find.

<br>

## Q4. What is a difference between workbook and worksheet
----------------------------------------------

In Microsoft Excel, a workbook is a collection of one or more spreadsheets, also called worksheets, in a single file. Below is an example of a spreadsheet called "Sheet1" in an Excel workbook file called "Book1." Our example also has the "Sheet2" and "Sheet3" sheet tabs, which are also part of the same workbook.

> Difference Between Workbook and Worksheet:

|**Worksheet**|**Workbook**|
|:-----------:|:----------:|
|The Worksheet is a single-page spreadsheet.|A workbook is just a file or a book.|
|It consists of a matrix of rectangular cells, organized in a tabular form of rows and columns.|The workbook consists of one or more worksheets, having various sorts of related information.|
|It is easy to add multiple worksheets to a workbook.|Adding a workbook to another workbook isn’t an easy task.|
|The worksheet is specific for a set of data.|A Workbook is the general form of data.|
|Practically, there is no limit for worksheets that can be kept in a workbook.|But Workbook has a limitation on the number of data entered|
|Worksheets are most preferred in an educational or learning environment|It is used to work in a professional environment.|
|A worksheet can also be converted into a workbook|But the workbook can be automatically created within a worksheet|
|Data manipulation and analysis is only possible with worksheets and not workbooks|Data manipulation is not possible with a workbook.|
|Using a separate worksheet for different tasks can be complicated or become problematic.|Many worksheets can be used at the same time in a workbook|

<br>

## Q5. How to start powerpoint
------------------------------

The way you launch computer programs determines how productive you are. If you click your Windows "Start" button and search for PowerPoint every time you want to launch it, you may be wasting time you could use to perform other tasks. Windows 7 provides many methods to open applications such as PowerPoint instantly via mouse clicks or even shortcut keys. Choose one that works best with your workflow.

> Launch Using All Programs

1. Click the Windows "Start" button, and then select “All Programs.”
2. Scroll through the list of folders that appear to find the folder labeled “Microsoft Office.”
3. Click that folder, and then click the “Microsoft PowerPoint” icon to open PowerPoint.

> Launch from Start Menu Shortcut

1. Find the “Microsoft PowerPoint” icon as described in the previous section.
2. Right-click the icon to view a menu containing options
3. Select “Pin to Start Menu.” Windows places a copy of the icon on your Start Menu. Use the icon to launch PowerPoint from the Start menu.

> Launch from Taskbar

1. Locate the “Microsoft PowerPoint” icon as described in the previous section.
2. Right-click the icon.
3. Select the “Pin to Taskbar” option that appears in the menu. A PowerPoint icon appears on the taskbar. Use this icon to launch PowerPoint from the taskbar.

> Launch from Desktop

1. Find the “Microsoft PowerPoint” icon as described in the previous sections.
2. Right-click the icon, and then select “Copy.”
3. Right-click a blank area on your desktop, and then select “Paste Shortcut.” Windows places a shortcut to PowerPoint on the desktop. Use this desktop icon to launch PowerPoint.

> Launch using Shortcut Key

1. Right-click the PowerPoint desktop icon created in the previous section.
2. Select “Properties,” and then click the "Shortcut tab" that appears in the Properties window.
3. Click inside the “Shortcut” text box and type a number or letter on your keyboard.

<br>

# Ms Access

## Q1. What do you understand by DBMS and RDMS?

- Database Management System (DBMS) is a software that is used to define, create and maintain a database and provides controlled access to the data.

- Relational Database Management System (RDBMS) is an advanced version of a DBMS. 

|**DBMS**|**RDBMS**|
|:------:|:-------:|
|DBMS stores data as file.|DBMS stores data as file.|
|Data elements need to access individually.|Multiple data elements can be accessed at the same time.|
|No relationship between data.|Data is stored in the form of tables which are related to each other.|
|Normalization is not present.|Normalization is present.|
|DBMS does not support distributed database.|RDBMS supports distributed database.|
|It stores data in either a navigational or hierarchical form.|It uses a tabular structure where the headers are the column names, and the rows contain corresponding values.|
|It deals with small quantity of data.|It deals with large amount of data.|
|Data redundancy is common in this model.|Keys and indexes do not allow Data redundancy.|
|It is used for small organization and deal with small data.|It is used to handle large amount of data.|
|It supports single user.|It supports multiple users.|
|Data fetching is slower for the large amount of data.|Data fetching is fast because of relational approach.|
|The data in a DBMS is subject to low security levels with regards to data manipulation.|There exists multiple levels of data security in a RDBMS.|
|Low software and hardware necessities.|Higher software and hardware necessities.|
|Examples: XML, Window Registry, etc.|Examples: MySQL, PostgreSQL, SQL Server, Oracle, Microsoft Access etc.|

<br>

## Q2. Explain the process of creating tables in MS Access?

In Access, you have several options when creating tables. You can create a new blank table from scratch or set up tables that are connected to SharePoint lists. If you use one of the Quick Start options in the Application Parts to create a new database, tables with predefined fields will be created.

> Create a New Blank Table

1. Click the Create tab.

2. Click Table.

*A new table appears in Datasheet View. You can start entering data right away, but you should add some fields first.*

3. Click the Click to Add field heading.

*A list of data types appears. See the table at the end of this lesson to learn more about your options.*

4. Select the field type.

**Tip** *When selecting a field type, select the smallest or shortest field that is required for your data. For example, don’t choose Large Number if you only need to store a 2-digit number.*

5. Type a name for the field.

**Tip** *To change the name of a field header, double-click the field header and type the field name you want to use.*

6. Repeat Steps 3-5 to add the remaining fields to your table.

7. When you’re finished adding fields, click the Close button and click Yes to save your changes.

8. Enter a name for your new table.

9. Click OK.

> Create a Table in Design View

**Design View allows you to create a table with more ease.**

1. Click the Create tab.

2. Click Table Design.

*A new table appears in the window in Design View.*

3. Enter a field name in the Field Name column and press Enter.

4. Click the Data Type list arrow and select a data type for the field.

*See the table at the end of this lesson for more information about data types.*

5. Repeat steps 3-4 to add as many fields as you want.

6. When you’re finished, click the Close button. Save and name your table.

*That’s all there is to creating a table!*

> Create a Table from Application Parts

**You can also create a table using an application part template. An Application Part is a predefined part of a database, such as a table or form, that you can quickly insert and use in a database.**

1. Click the Create tab on the ribbon.

2. Click Application Parts.

*The application parts menu appears. The Quick Start section contains templates for creating tables.*

3. Select a template from the Quick Start section.

*The Create Relationship wizard appears, where you can enter information about the new table.*

4. Complete the steps in the Create Relationship wizard.

5. When you’re finished, click the Close button. Save and name your table 

*Access creates a new table from the application part template you selected.*

> Create a SharePoint List Table

**SharePoint lists can be created within Access. To do this, you need to have an existing SharePoint site that you can add the SharePoint list too.**

1. Click the Create tab on the ribbon.

2. Click the SharePoint Lists button in the Tables group.

3. Select the relevant type of list you want to create – either Contacts, Tasks, Issues or Events.

*Select Custom if you wish to specify your own fields or choose Existing SharePoint List if you want to link to a list that has previously been created on the SharePoint site.*

4. Enter the SharePoint URL address, a name for the new list, and any other information you’d like.

5. Click OK.

<br>

## Q3. Differentiate between query and filter
----------------------------------------------

* **Queries**: A query calculates how relevant each document is to the query, and assigns it a relevance score, which is later used to sort matching documents by relevance. This concept of relevance is well suited to full-text search, where there is seldom a completely “correct” answer. The query also asks the question like below: 
    
    - How well does this document match?
    - What is the created adate of Elasticsearch?
    - What is the distance of lat_lon field from the specified point?

* **Filters**: The output from most filter clauses is a simple list of the documents that match the filter. It is quick to calculate and easy to cache in memory, using only 1 bit per document. These cached filters can be reused efficiently for subsequent requests. A filter asks a yes or no question of every document and is used for fields that contain exact values:

    - Is the created date in the range 2013 – 2014?
    - Does the status field contain the term published?
    - Is the lat_lon field within 10km of a specified point?

> Differences between Queries and Filters: 

|**Queries**|**Filters**|
|:---------:|:---------:|
|Queries are slower it returns a calculated score of how well a document matches the query.|Filters are faster because they check only if the document matched or not.|
|Queries produce non-boolean values.|Filters produce boolean values.|
|Using filters after performing a Query is faster compare to others.|But using a Query after filter is not worth it.|
|Queries are not cacheable.|Filters are cacheable.|
