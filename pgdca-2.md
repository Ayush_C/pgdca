# PGDCA - 2

## IT

### Short note 

> Plain text

[**LINK**](https://en.wikipedia.org/wiki/Plain_text)


In computing, plain text is a loose term for data (e.g. file contents) that represent only characters of readable material but not its graphical representation nor other objects (floating-point numbers, images, etc.). It may also include a limited number of "whitespace" characters that affect simple arrangement of text, such as spaces, line breaks, or tabulation characters (although tab characters can "mean" many different things, so are hardly "plain"). Plain text is different from formatted text, where style information is included; from structured text, where structural parts of the document such as paragraphs, sections, and the like are identified; and from binary files in which some portions must be interpreted as binary objects (encoded integers, real numbers, images, etc.).

The term is sometimes used quite loosely, to mean files that contain only "readable" content (or just files with nothing that the speaker doesn't prefer). For example, that could exclude any indication of fonts or layout (such as markup, markdown, or even tabs); characters such as curly quotes, non-breaking spaces, soft hyphens, em dashes, and/or ligatures; or other things.

In principle, plain text can be in any encoding, but occasionally the term is taken to imply ASCII. As Unicode-based encodings such as UTF-8 and UTF-16 become more common, that usage may be shrinking.

Plain text is also sometimes used only to exclude "binary" files: those in which at least some parts of the file cannot be correctly interpreted via the character encoding in effect. For example, a file or string consisting of "hello" (in whatever encoding), following by 4 bytes that express a binary integer that is not just a character(s), is a binary file, not plain text by even the loosest common usages. Put another way, translating a plain text file to a character encoding that uses entirely different numbers to represent characters does not change the meaning (so long as you know what encoding is in use), but for binary files such a conversion does change the meaning of at least some parts of the file.


> Formatted text

[**LINK**](https://www.computerhope.com/jargon/f/formatted-text.htm)


Formatted text is text that is displayed in a special, specified style. In computer applications, formatting data may be associated with text data to create formatted text. How formatted text is created and displayed is dependent on the operating system and application software used on the computer.

Text formatting data may be qualitative (e.g., font family), or quantitative (e.g., font size, or color). It may also indicate a style of emphasis (e.g., boldface, or italics), or a style of notation (e.g., strikethrough, or superscript).


> RTF

[**LINK**](https://www.techtarget.com/whatis/definition/Rich-Text-Format-RTF)


Rich Text Format (RTF) is a file format that lets you exchange text files between different word processors in different operating systems. For example, you can create a file using Microsoft Word in Windows 98, save it as an RTF file (it will have a ".rtf" file name suffix), and send it to someone who uses WordPerfect 6.0 on Windows 3.1 and they will be able to open the file and read it. (In some cases, the RTF capability may be built into the word processor. In others, a separate reader or writer may be required.)

The RTF Specification uses the ANSI, PC-8, Macintosh, and IBM PC character sets. It defines control words and symbols that serve as "common denominator" formatting commands. When saving a file in the Rich Text Format, the file is processed by an RTF writer that converts the word processor's markup to the RTF language. When being read, the control words and symbols are processed by an RTF reader that converts the RTF language into formatting for the word processor that will display the document. The Specification, a copy of which is located in the archives at the World Wide Web Consortium, is used to create an RTF reader or writer.


> HTML

[**LINK**](https://en.wikipedia.org/wiki/HTML)


The HyperText Markup Language or HTML is the standard markup language for documents designed to be displayed in a web browser. It can be assisted by technologies such as Cascading Style Sheets (CSS) and scripting languages such as JavaScript.

Web browsers receive HTML documents from a web server or from local storage and render the documents into multimedia web pages. HTML describes the structure of a web page semantically and originally included cues for the appearance of the document.

HTML elements are the building blocks of HTML pages. With HTML constructs, images and other objects such as interactive forms may be embedded into the rendered page. HTML provides a means to create structured documents by denoting structural semantics for text such as headings, paragraphs, lists, links, quotes and other items. HTML elements are delineated by tags, written using angle brackets. Tags such as <img /> and <input /> directly introduce content into the page. Other tags such as <p> surround and provide information about document text and may include other tags as sub-elements. Browsers do not display the HTML tags but use them to interpret the content of the page.

HTML can embed programs written in a scripting language such as JavaScript, which affects the behavior and content of web pages. Inclusion of CSS defines the look and layout of content. The World Wide Web Consortium (W3C), former maintainer of the HTML and current maintainer of the CSS standards, has encouraged the use of CSS over explicit presentational HTML since 1997.[2] A form of HTML, known as HTML5, is used to display video and audio, primarily using the <canvas> element, in collaboration with javascript.


### Sound file formate

[**LINK**](https://www.canto.com/blog/audio-file-types/)

1. M4A

The M4A is an mpeg-4 audio file. It is an audio-compressed file used in the modern setting due to increased quality demand as a result of cloud storage and bigger hard drive space in contemporary computers. Its high quality keeps it relevant, as users who need to hear distinct sounds on audio files will need this over more common file types.

Music download software like Apple iTunes use M4A instead of MP3 because it’s smaller in size and higher in quality. Its limitations come in the form of compatibility, as a lot of software are unable to recognize the M4A, making it ideal for only a select type of user.


2. FLAC

The FLAC audio file is Free Lossless Audio Codec. It is an audio file compressed into a smaller size of the original file. It’s a sophisticated file type that is lesser-used among audio formats. This is because, even though it has its advantages, it often needs special downloads to function. When you consider that audio files are shared often, this can make for quite an inconvenience to each new user who receives one.

What makes the FLAC so important is the lossless compression can save size and promote sharing of an audio file while being able to return to the original quality standard. The near-exact amount of storage space required of the original audio file is sixty percent – this saves a lot of hard drive space and time spent uploading or downloading.


3. MP3

The MP3 audio file is an MPEG audio layer 3 file format. The key feature of MP3 files is the compression that saves valuable space while maintaining near-flawless quality of the original source of sound. This compression makes the MP3 very popular for all mobile audio-playing devices, particularly the Apple iPod.

MP3 continues to be relevant in today’s digital landscape because it’s compatible with nearly every device capable of reading audio files. The MP3 is probably best used for extensive audio file sharing due to its manageable size. It also works well for websites that host audio files. Finally, the MP3 remains popular because of its overall sound quality. Though not the highest quality, it has enough other benefits to compensate.


4. MP4

An MP4 audio file is often mistaken as an improved version of the MP3 file. However, this couldn’t be further from the truth. The two are completely different and the similarities come from their namesake rather than their functionality. Also note that the MP4 is sometimes referred to as a video file instead of an audio file. This isn’t an error, as in fact it’s both an audio and video file.

An MP4 audio file type is a comprehensive media extension, capable of holding audio, video and other media. The MP4 contains data in the file, rather than code. This is important to note as MP4 files require different codecs to implement the code artificially and allow it to be read.


5. WAV

A WAV audio file is a Waveform Audio File that stores waveform data. The waveform data stored presents an image that demonstrates strength of volume and sound in specific parts of the WAV file. It is entirely possible to transform a WAV file using compression, though it’s not standard. Also, the WAV is typically used on Windows systems.

The easiest way to envision this concept is by thinking of ocean waves. The water is loudest, fullest and strongest when the wave is high. The same holds true for the waveform in the WAV. The visuals are high and large when the sound increases in the file. WAV files are usually uncompressed audio files, though it’s not a requirement of the format.


6. WMA

The WMA (Windows Media Audio) is a Windows-based alternative to the more common and popular MP3 file type. What makes so beneficial is its lossless compression, retaining high audio quality throughout all types of restructuring processes. Even though it’s such a quality audio format, it’s not the most popular due to the fact it’s inaccessible to many users, especially those who don’t use the Windows operating system.

If you’re a Windows user, simply double-click any WMA file to open it. The file will open with Windows Media Player (unless you’ve changed the default program). If you’re not using Windows, there are some alternatives to help you out. The first option is to download a third-party system that plays the WMA. If this isn’t something you want to do, consider converting the WMA to a different audio format. There are plenty of conversion tools available.


7. AAC

The AAC (Advanced Audio Coding) is an audio file that delivers decently high-quality sound and is enhanced using advanced coding. It has never been one of the most popular audio formats, especially when it comes to music files, but the AAC does still serve some purpose for major systems. This includes popular mobile devices and video gaming units, where the AAC is a standard audio component.

To open an AAC file, the most common and direct format for most users is through iTunes. All this entails is launching the iTunes system and opening the AAC file from your computer in the ‘File’ menu. If you don’t have iTunes and want an alternative, consider downloading third-party software capable of opening the AAC. If that doesn’t suit your needs, convert the AAC to a more common audio file type.


### E-Governance

[**LINK-1**](https://cleartax.in/s/e-governance)<br>
[**LINK-2**](https://www.igi-global.com/dictionary/cyber-capability-framework/8702)

* **E-governance in India** - The application of information technology and communication for the purpose of governance is commonly known as e-governance. Through e-governance, information can be distributed to the public in a transparent manner.

* **What is e-Governance?** - Electronic governance or e-governance is adopted by countries across the world. In a fast-growing and demanding economy like India, e-governance has become essential.  The rapid growth of digitalisation has led to many governments across the globe to introduce and incorporate technology into governmental processes. Electronic governance or e-governance can be defined as the usage of Information and Communication Technology (ICT) by the government to provide and facilitate government services, exchange of information, communication transactions and integration of various standalone systems and services. In other words, it is the use of technology to perform government activities and achieve the objectives of governance.  Through e-governance, government services are made available to citizens and businesses in a convenient, efficient and transparent manner. Examples of e-governance include Digital India initiative, National Portal of India, Prime Minister of India portal, Aadhaar, filing and payment of taxes online, digital land management systems, Common Entrance Test etc.

* **Types of interactions in e-Governance :**
    
    - Government to Government (G2G) 
    - Government to Citizen (G2C) 
    - Government to Businesses (G2B) 
    - Government to Employees (G2E) 

* **Objectives of e-Governance** 

    - To support and simplify governance for government, citizens, and businesses. 
    - To make government administration more transparent and accountable while addressing the society’s needs and expectations through efficient public services and effective interaction between the people, businesses, and government. 
    - To reduce corruption in the government. 
    - To ensure speedy administration of services and information. 
    - To reduce difficulties for business, provide immediate information and enable digital communication by e-business.

### Wireless Communication

> What is Wireless Communication?

[**LINK**](https://www.electronicshub.org/wireless-communication-introduction-types-applications/)


Communication Systems can be Wired or Wireless and the medium used for communication can be Guided or Unguided. In Wired Communication, the medium is a physical path like Co-axial Cables, Twisted Pair Cables and Optical Fiber Links etc. which guides the signal to propagate from one point to other.

Such type of medium is called Guided Medium. On the other hand, Wireless Communication doesn’t require any physical medium but propagates the signal through space. Since, space only allows for signal transmission without any guidance, the medium used in Wireless Communication is called Unguided Medium.

If there is no physical medium, then how does wireless communication transmit signals? Even though there are no cables used in wireless communication, the transmission and reception of signals is accomplished with Antennas.

Antennas are electrical devices that transform the electrical signals to radio signals in the form of Electromagnetic (EM) Waves and vice versa. These Electromagnetic Waves propagates through space. Hence, both transmitter and receiver consists of an antenna.


> Types

[**LINK**](https://www.rantecantennas.com/blog/the-different-types-of-wireless-communication/)

```
1. Satellite Communication
2. Infrared Communication
3. Broadcast Radio
4. Microwave Communication
5. Wi-Fi
6. Mobile Communication Systems
7. Bluetooth Technology
```

### Expert System

[**LINK**](https://en.wikipedia.org/wiki/Expert_system)

> Meaning


In artificial intelligence, an expert system is a computer system emulating the decision-making ability of a human expert.Expert systems are designed to solve complex problems by reasoning through bodies of knowledge, represented mainly as if–then rules rather than through conventional procedural code. The first expert systems were created in the 1970s and then proliferated in the 1980s. Expert systems were among the first truly successful forms of artificial intelligence (AI) software. An expert system is divided into two subsystems: the inference engine and the knowledge base. The knowledge base represents facts and rules. The inference engine applies the rules to the known facts to deduce new facts. Inference engines can also include explanation and debugging abilities.


> Advantages


The goal of knowledge-based systems is to make the critical information required for the system to work explicit rather than implicit. In a traditional computer program the logic is embedded in code that can typically only be reviewed by an IT specialist. With an expert system the goal was to specify the rules in a format that was intuitive and easily understood, reviewed, and even edited by domain experts rather than IT experts. The benefits of this explicit knowledge representation were rapid development and ease of maintenance.

Ease of maintenance is the most obvious benefit. This was achieved in two ways. First, by removing the need to write conventional code, many of the normal problems that can be caused by even small changes to a system could be avoided with expert systems. Essentially, the logical flow of the program (at least at the highest level) was simply a given for the system, simply invoke the inference engine. This also was a reason for the second benefit: rapid prototyping. With an expert system shell it was possible to enter a few rules and have a prototype developed in days rather than the months or year typically associated with complex IT projects.

A claim for expert system shells that was often made was that they removed the need for trained programmers and that experts could develop systems themselves. In reality, this was seldom if ever true. While the rules for an expert system were more comprehensible than typical computer code, they still had a formal syntax where a misplaced comma or other character could cause havoc as with any other computer language. Also, as expert systems moved from prototypes in the lab to deployment in the business world, issues of integration and maintenance became far more critical. Inevitably demands to integrate with, and take advantage of, large legacy databases and systems arose. To accomplish this, integration required the same skills as any other type of system.

Summing up the benefits of using expert systems, the following can be highlighted:

1. Increased availability and reliability: Expertise can be accessed on any computer hardware and the system always completes responses on time.
2. Multiple expertise: Several expert systems can be run simultaneously to solve a problem. and gain a higher level of expertise than a human expert.
3. Explanation: Expert systems always describe of how the problem was solved.
4. Fast response: The expert systems are fast and able to solve a problem in real-time.
5. Reduced cost: The cost of expertise for each user is significantly reduced.

> Application

|Category|	Problem addressed|	Examples|
|:-:|:-:|:-:|
|**Interpretation**|	Inferring situation descriptions from sensor data|	Hearsay (speech recognition), PROSPECTOR
|**Prediction**|	Inferring likely consequences of given situations|	Preterm Birth Risk Assessment
|**Diagnosis**|	Inferring system malfunctions from observables|	CADUCEUS, MYCIN, PUFF, Mistral, Eydenet, Kaleidos
|**Design**|	Configuring objects under constraints|	Dendral, Mortgage Loan Advisor, R1 (DEC VAX Configuration), SID (DEC VAX 9000 CPU)
|**Planning**|	Designing actions|	Mission Planning for Autonomous Underwater Vehicle
|**Monitoring**|	Comparing observations to plan vulnerabilities|	REACTOR
|**Debugging**|	Providing incremental solutions for complex problems|	SAINT, MATHLAB, MACSYMA
|**Repair**|	Executing a plan to administer a prescribed remedy|	Toxic Spill Crisis Management
|**Instruction**|	Diagnosing, assessing, and correcting student behaviour|	SMH.PAL, Intelligent Clinical Training, STEAMER
|**Control**|	Interpreting, predicting, repairing, and monitoring system behaviors|	Real Time Process Control, Space Shuttle Mission Control, Smart Autoclave Cure of Composites

## Internet

### WWW (meaning)

[**LINK**](https://en.wikipedia.org/wiki/World_Wide_Web)


The World Wide Web (WWW), commonly known as the Web, is the world's dominant software platform. It is an information space where documents and other web resources can be accessed through the Internet using a web browser. The Web has changed people's lives immeasurably. It is the primary tool billions of people worldwide use to interact on the Internet.

Web resources may be any type of downloadable media. Web pages are documents interconnected by hypertext links formatted in Hypertext Markup Language (HTML). The HTML syntax displays embedded hyperlinks with URLs, which permits users to navigate to other web resources. In addition to text, web pages may contain references to images, video, audio, and software components, which are either displayed or internally executed in the user's web browser to render pages or streams of multimedia content. Web applications are web pages that function as application software.

Multiple web resources with a common theme and usually a common domain name make up a website. Websites are stored in computers that are running a web server, which is a program that responds to requests made over the Internet from web browsers running on a user's computer. Website content can be provided by a publisher or interactively from user-generated content. Websites are provided for a myriad of informative, entertainment, commercial, and governmental reasons.

The Web was originally conceived as a document management system. The information in the Web is transferred via the Hypertext Transfer Protocol (HTTP) to be accessed by users through software applications.


### WWW (Function)

[**LINK**](https://www.geeksforgeeks.org/world-wide-web-www/#:~:text=Working%20of%20WWW%3A,and%20video%20on%20the%20Internet.)


The World Wide Web is based on several different technologies: Web browsers, Hypertext Markup Language (HTML) and Hypertext Transfer Protocol (HTTP). 

A Web browser is used to access webpages. Web browsers can be defined as programs which display text, data, pictures, animation and video on the Internet. Hyperlinked resources on the World Wide Web can be accessed using software interface provided by Web browsers. Initially Web browsers were used only for surfing the Web but now they have become more universal. Web browsers can be used for several tasks including conducting searches, mailing, transferring files, and much more. Some of the commonly used browsers are Internet Explorer, Opera Mini, Google Chrome. 


### Short Note

> Google maps

[**LINK**](https://en.wikipedia.org/wiki/Google_Maps)

Google Maps is a web mapping platform and consumer application offered by Google. It offers satellite imagery, aerial photography, street maps, 360° interactive panoramic views of streets (Street View), real-time traffic conditions, and route planning for traveling by foot, car, bike, air (in beta) and public transportation. As of 2020, Google Maps was being used by over 1 billion people every month around the world.

Google Maps began as a C++ desktop program developed by brothers Lars and Jens Rasmussen at Where 2 Technologies. In October 2004, the company was acquired by Google, which converted it into a web application. After additional acquisitions of a geospatial data visualization company and a real time traffic analyzer, Google Maps was launched in February 2005. The service's front end utilizes JavaScript, XML, and Ajax. Google Maps offers an API that allows maps to be embedded on third-party websites, and offers a locator for businesses and other organizations in numerous countries around the world. Google Map Maker allowed users to collaboratively expand and update the service's mapping worldwide but was discontinued from March 2017. However, crowdsourced contributions to Google Maps were not discontinued as the company announced those features would be transferred to the Google Local Guides program.

> Online train booking

[**Source**](https://www.intel.com/content/dam/www/program/education/us/en/documents/intel-easy-steps/easy-steps-make-online-railway-reservations.pdf)


> Justdial

[**LINK**](https://www.justdial.com/cms/overview)

Just Dial Limited is India's No. 1 Local Search engine that provides local search related services to users across India through multiple platforms such as website, mobile website, Apps (Android, iOS), over the telephone (voice, pan India number 8888888888) and text (SMS).Justdial has also initiated ‘Search Plus’ services for its users. These services aim at making several day-to-day tasks conveniently actionable and accessible to users through one App. By doing so, it has transitioned from being purely a provider of local search and related information to being an enabler of such transactions. Justdial has also recently launched JD Omni, an end-to-end business management solution for SMEs, through which it intends to transition thousands of SMEs to efficiently run their business online and have adequate online presence via their own website and mobile site. Apart from this, it has also launched JD Pay, a unique solution for quick digital payments for its users and vendors, and JD Social, its official social sharing platform to provide curated content on latest happenings to users. The organisation also aims to make communication between users and businesses seamless through its Real Time Chat Messenger.

### Character formatting HTML

Character format elements are used to specify either the logical meaning or the physical appearance of marked text without causing a paragraph break. Like most other elements, character format elements include both opening and closing tags.

**HTML Formatting Elements**

[**LINK**](https://www.w3schools.com/html/html_formatting.asp)

```
<b> - Bold text
<strong> - Important text
<i> - Italic text
<em> - Emphasized text
<mark> - Marked text
<small> - Smaller text
<del> - Deleted text
<ins> - Inserted text
<sub> - Subscript text
<sup> - Superscript text
```

### What is JavaScript?

[**LINK**](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/First_steps/What_is_JavaScript)

JavaScript is a scripting or programming language that allows you to implement complex features on web pages — every time a web page does more than just sit there and display static information for you to look at — displaying timely content updates, interactive maps, animated 2D/3D graphics, scrolling video jukeboxes, etc. — you can bet that JavaScript is probably involved. It is the third layer of the layer cake of standard web technologies, two of which (HTML and CSS) we have covered in much more detail in other parts of the Learning Area.

**JavaScript variable**

[**LINK**](https://www.javatpoint.com/javascript-variable#:~:text=A%20JavaScript%20variable%20is%20simply,or%20dollar(%20%24%20)%20sign.)

A JavaScript variable is simply a name of storage location. There are two types of variables in JavaScript : local variable and global variable.

There are some rules while declaring a JavaScript variable (also known as identifiers).

1. Name must start with a letter (a to z or A to Z), underscore( _ ), or dollar( $ ) sign.
2. After first letter we can use digits (0 to 9), for example value1.
3. JavaScript variables are case sensitive, for example x and X are different variables.

### E-Commerce

[**LINK**](https://www.toppr.com/guides/business-environment/emerging-trends-in-business/electronic-commerce/#:~:text=E%2DCommerce%20or%20Electronic%20Commerce,also%20considered%20as%20E%2Dcommerce.)

**MEANING**

E-Commerce or Electronic Commerce means buying and selling of goods, products, or services over the internet. E-commerce is also known as electronic commerce or internet commerce. These services provided online over the internet network. Transaction of money, funds, and data are also considered as E-commerce. These business transactions can be done in four ways: Business to Business (B2B), Business to Customer (B2C), Customer to Customer (C2C), Customer to Business (C2B). The standard definition of E-commerce is a commercial transaction which is happened over the internet. Online stores like Amazon, Flipkart, Shopify, Myntra, Ebay, Quikr, Olx are examples of E-commerce websites. By 2020, global retail e-commerce can reach up to $27 Trillion. Let us learn in detail about what is the advantages and disadvantages of E-commerce and its types.

**TYPES**

- Business to Business
- Business to Consumer
- Consumer to Consumer
- Consumer to Business

**EXAMPLES**
```
Amazon
Flipkart
eBay
Fiverr
Upwork
Olx
Quikr
```

**Advantages of E-Commerce**

- E-commerce provides the sellers with a global reach. They remove the barrier of place (geography). Now sellers and buyers can meet in the virtual world, without the hindrance of location.

- Electronic commerce will substantially lower the transaction cost. It eliminates many fixed costs of maintaining brick and mortar shops. This allows the companies to enjoy a much higher margin of profit.

- It provides quick delivery of goods with very little effort on part of the customer. Customer complaints are also addressed quickly. It also saves time, energy and effort for both the consumers and the company.

- One other great advantage is the convenience it offers. A customer can shop 24×7. The website is functional at all times, it does not have working hours like a shop.

- Electronic commerce also allows the customer and the business to be in touch directly, without any intermediaries. This allows for quick communication and transactions. It also gives a valuable personal touch.

**Disadvantages of E-Commerce**

- The start-up costs of the e-commerce portal are very high.

- The setup of the hardware and the software, the training cost of employees, the constant maintenance and upkeep are all quite expensive.

- Although it may seem like a sure thing, the e-commerce industry has a high risk of failure. Many companies riding the dot-com wave of the 2000s have failed miserably. The high risk of failure remains even today.

- At times, e-commerce can feel impersonal. So it lacks the warmth of an interpersonal relationship which is important for many brands and products. This lack of a personal touch can be a disadvantage for many types of services and products like interior designing or the jewelry business.

- Security is another area of concern. Only recently, we have witnessed many security breaches where the information of the customers was stolen. Credit card theft, identity theft etc. remain big concerns with the customers.

- Then there are also fulfillment problems. Even after the order is placed there can be problems with shipping, delivery, mix-ups etc. This leaves the customers unhappy and dissatisfied.
